﻿using System;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.RabbitMq
{
    public interface IRabbitMqService
    {
        void GivePromoCodeToCustomerAsync(PromoCode promoCode);

        void NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId);
    }
}
